package blog

import (
	"gitlab.com/golang-demos/blog-api/src/domain"
)

////////////////////////////////////////
// Main service interface & constructor
////////////////////////////////////////
type Service interface {
	SavePost(req *SavePostRequest) (*SavePostResponse, error)
	GetPost(req *GetPostRequest) (*GetPostResponse, error)
	DeletePost(req *DeletePostRequest) (*DeletePostResponse, error)
	SaveToElastic(req *SavePostRequest) error
	GetFilteredPosts (req *GetPostRequest) ([]domain.Post, error)
	DeleteById (req *DeletePostRequest) (*DeletePostResponse, error)
}

type service struct {
	postCommandRepo domain.PostCommandRepo
	postQueryRepo domain.PostQueryRepo
}

func NewService(postCR domain.PostCommandRepo, PostQR domain.PostQueryRepo) Service {
	return &service{
		postCommandRepo: postCR,
		postQueryRepo: PostQR,
	}
}

/////////////////////////////////
// Blog service methods
/////////////////////////////////

// Create/Update post service method
func (s *service) SavePost(req *SavePostRequest) (*SavePostResponse, error) {

	post := req.Post

	if post.Uid == "" {
		post.GenerateUid()
	}

	err := s.postCommandRepo.Store(&post)
	if err != nil {
		return nil, err
	}

	return &SavePostResponse{PostId: post.Uid}, nil
}

// Get post service method
func (s *service) GetPost(req *GetPostRequest) (*GetPostResponse, error) {

	post, err := s.postCommandRepo.GetByID(req.PostId)
	if err != nil {
		return nil, err
	}

	return &GetPostResponse{*post}, nil
}

// Delete post service method
func (s *service) DeletePost(req *DeletePostRequest) (*DeletePostResponse, error) {

	err := s.postCommandRepo.DeleteById(req.PostId)
	if err != nil {
		return nil, err
	}

	return &DeletePostResponse{Msg: "post deleted successfully"}, nil
}

func (s *service) SaveToElastic(req *SavePostRequest) error{
	err := s.postQueryRepo.SaveToElastic(&req.Post)
	if err != nil {
		return err
	}
	return nil
}

func (s * service) GetFilteredPosts (req *GetPostRequest) ([]domain.Post, error){
	var posts []domain.Post
	posts, err := s.postQueryRepo.GetFilteredPosts(req.PostId)
	if err != nil {
		return nil, err
	}
	return posts, nil
}

func (s * service) DeleteById (req *DeletePostRequest) (*DeletePostResponse, error){
	err := s.postQueryRepo.DeleteById(req.PostId)
	if err != nil {
		return nil, err
	}
	return &DeletePostResponse{Msg:"deleted successfully"}, nil
}