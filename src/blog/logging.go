package blog

import (
	"github.com/go-kit/kit/log"
	"github.com/sirupsen/logrus"
	"time"
)

/////////////////////////////////
// Global variables
/////////////////////////////////
var Loger *logrus.Logger

/////////////////////////////////
// Logging functions
/////////////////////////////////

// Create/Update post logging service
func (s *loggingService) SavePost(req *SavePostRequest) (_ *SavePostResponse, err error) {

	defer func(begin time.Time) {
		if err != nil {
			_ = s.logger.Log(
				"method", "save_post",
				"took", time.Since(begin),
				"err", err,
			)
		}
	}(time.Now())

	return s.Service.SavePost(req)
}

// Get post logging service
func (s *loggingService) GetPost(req *GetPostRequest) (_ *GetPostResponse, err error) {

	defer func(begin time.Time) {
		if err != nil {
			_ = s.logger.Log(
				"method", "get_post",
				"took", time.Since(begin),
				"err", err,
			)
		}
	}(time.Now())

	return s.Service.GetPost(req)
}

// Delete post logging service
func (s *loggingService) DeletePost(req *DeletePostRequest) (_ *DeletePostResponse, err error) {

	defer func(begin time.Time) {
		if err != nil {
			_ = s.logger.Log(
				"method", "delete_post",
				"took", time.Since(begin),
				"err", err,
			)
		}
	}(time.Now())

	return s.Service.DeletePost(req)
}

/////////////////////////////////
// Additional functions
/////////////////////////////////
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

/////////////////////////////////
// Additional structures
/////////////////////////////////
type loggingService struct {
	logger log.Logger
	Service
}
