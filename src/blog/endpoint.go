package blog

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"gitlab.com/golang-demos/blog-api/src/domain"
	"gitlab.com/golang-demos/blog-api/src/errors"
)

/////////////////////////////////
// Blog endpoints
/////////////////////////////////

// Create/Update post req & resp
type SavePostRequest struct {
	domain.Post
}

type SavePostResponse struct {
	PostId string `json:"post_id"`
}

// Create/Update post endpoint
func makeSavePostEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(SavePostRequest)
		resp, err := s.SavePost(&req)
		if err != nil {
			return nil, err
		}

		return resp, nil
	}
}

// Get post req & resp
type GetPostRequest struct {
	PostId string `json:"post_id"`
}

type GetPostResponse struct {
	domain.Post
}


// Get post endpoint
func makeGetPostEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetPostRequest)
		resp, err := s.GetPost(&req)
		if err != nil {
			return nil, errors.NoContentFound.DevMessage(err.Error())
		}

		return resp, nil
	}
}

func makeGetPostToElasticEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(SavePostRequest)
		err := s.SaveToElastic(&req)
		if err != nil {
			return nil, errors.NoContentFound.DevMessage(err.Error())
		}

		return req, nil
	}
}


func makeFilterPostEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetPostRequest)
		resp, err := s.GetFilteredPosts(&req)
		if err != nil {
			return nil, err
		}

		return resp, nil
	}
}

func makeDeletePostElasticEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeletePostRequest)
		resp, err := s.DeleteById(&req)
		if err != nil {
			return nil, err
		}

		return resp, nil
	}
}

// Delete post req & resp
type DeletePostRequest struct {
	PostId string `json:"post_id"`
}

type DeletePostResponse struct {
	Msg string `json:"msg"`
}

// Delete post endpoint
func makeDeletePostEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeletePostRequest)
		resp, err := s.DeletePost(&req)
		if err != nil {
			return nil, err
		}

		return resp, nil
	}
}
