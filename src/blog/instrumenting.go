package blog

import (
	"github.com/go-kit/kit/metrics"
	"time"
)

/////////////////////////////////
// Instrumenting functions
/////////////////////////////////

// Create/Update post instrumenting service
func (s *instrumentingService) SavePost(req *SavePostRequest) (_ *SavePostResponse, err error) {

	defer func(begin time.Time) {
		s.requestCount.With("method", "save_post").Add(1)
		if err != nil {
			s.requestError.With("method", "save_post").Add(1)
		}
		s.requestLatency.With("method", "save_post").Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.SavePost(req)
}

// Get post instrumenting service
func (s *instrumentingService) GetPost(req *GetPostRequest) (_ *GetPostResponse, err error) {

	defer func(begin time.Time) {
		s.requestCount.With("method", "get_post").Add(1)
		if err != nil {
			s.requestError.With("method", "get_post").Add(1)
		}
		s.requestLatency.With("method", "get_post").Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetPost(req)
}

// Delete post instrumenting service
func (s *instrumentingService) DeletePost(req *DeletePostRequest) (_ *DeletePostResponse, err error) {

	defer func(begin time.Time) {
		s.requestCount.With("method", "delete_post").Add(1)
		if err != nil {
			s.requestError.With("method", "delete_post").Add(1)
		}
		s.requestLatency.With("method", "delete_post").Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DeletePost(req)
}

/////////////////////////////////
// Additional functions
/////////////////////////////////
func NewInstrumentingService(counter metrics.Counter, latency metrics.Histogram, counterE metrics.Counter, s Service) Service {
	return &instrumentingService{
		requestCount:   counter,
		requestLatency: latency,
		requestError:   counterE,
		Service:        s,
	}
}

/////////////////////////////////
// Additional structures
/////////////////////////////////
type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	requestError   metrics.Counter
	Service
}
