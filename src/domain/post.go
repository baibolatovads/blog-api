package domain

import "github.com/google/uuid"

/////////////////////////////////
// Main structure & methods
/////////////////////////////////
type Post struct {
	Uid              string   `json:"uid,omitempty"`
	CategoryId       string   `json:"category_id"`
	AuthorId         string   `json:"author_id"`
	Title            string   `json:"title"`
	ShortDescription string   `json:"short_description,omitempty"`
	PreviewUrl       string   `json:"preview_url,omitempty"`
	Content          string   `json:"content"`
	Tags             []string `json:"tags,omitempty"`
}

// Generate post unique ID
func (p *Post) GenerateUid() {
	p.Uid = uuid.New().String()
}

/////////////////////////////////
// Repository interfaces
/////////////////////////////////
type PostCommandRepo interface {
	Store(post *Post) error
	GetByID(id string) (*Post, error)
	DeleteById(id string) error
}

type PostQueryRepo interface{
	SaveToElastic(post *Post) error
	GetFilteredPosts (id string) ([]Post, error)
	DeleteById(uid string) error
}