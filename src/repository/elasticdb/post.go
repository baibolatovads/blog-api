package elasticdb

import (
	"context"
	"encoding/json"
	"gitlab.com/golang-demos/blog-api/src/domain"
	"gitlab.com/golang-demos/blog-api/src/errors"
	"gopkg.in/olivere/elastic.v5"
)

type PostQueryRepo struct{
	client *elastic.Client
}

func NewPostQueryRepo(client *elastic.Client) domain.PostQueryRepo{
	return &PostQueryRepo{client: client}
}

func (r *PostQueryRepo) SaveToElastic (post *domain.Post) error{
	_, err := r.client.Index().
		Index("posts").
		Type("post").
		Id(post.Uid).
		BodyJson(post).
		Refresh("true").
		Do(context.TODO())

	if err != nil {
		return errors.ElasticSaveError.DevMessage(err.Error())
	}
	return nil
}

func (r *PostQueryRepo) DeleteById(uid string) error {
	_, err := r.client.Delete().Index("posts").Type("post").Id(uid).Do(context.TODO())
	if err != nil {
		return errors.ElasticConnectError.DevMessage(err.Error())
	}
	return nil
}

func (r *PostQueryRepo) GetFilteredPosts(id string) ([]domain.Post, error){
	query := elastic.NewBoolQuery()
	searchQuery := r.client.Search()
	if id != ""{
		termQuery := elastic.NewTermQuery("post.id", id)
		query = query.Must(termQuery)
	} else{
		searchQuery = searchQuery.Sort("payload", false)
	}
	search, err := searchQuery.Index("posts").Type("post").Query(query).Do(context.TODO())
	if err != nil{
		msg := errors.ElasticConnectError.DevMessage(err.Error())
		return nil, msg
	}
	var posts []domain.Post
	if search.Hits.TotalHits > 0 {
		for _, hit := range search.Hits.Hits{
			var t domain.Post
			err := json.Unmarshal(*hit.Source, &t)
			if err != nil {
				msg := errors.DeserializeBug.DevMessage(err.Error())
				return nil, msg
			}
			t.Uid = hit.Id
			posts = append(posts, t)
		}
	} else{
		msg := errors.NoContentFound.DevMessage(err.Error())
		return nil, msg
	}
	return posts, nil
}






