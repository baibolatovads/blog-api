package scylladb

import (
	//"context"
	"encoding/json"
	"github.com/gocql/gocql"
	//"gitlab.com/golang-demos/blog-api/src/config"
	"gitlab.com/golang-demos/blog-api/src/domain"
	"gitlab.com/golang-demos/blog-api/src/errors"
)

type postCommandRepo struct {
	dbSession *gocql.Session
}

func NewPostCommandRepo(session *gocql.Session) domain.PostCommandRepo {
	return &postCommandRepo{dbSession: session}
}

// Store post to ScyllaDB
func (r *postCommandRepo) Store(post *domain.Post) error {

	postJson, err := json.Marshal(post)
	if err != nil {
		return errors.DeserializeBug.DevMessage(err.Error())
	}

	err = r.dbSession.Query("INSERT INTO posts (id, payload) VALUES (?, ?)",
		post.Uid,
		string(postJson)).
		Exec()
	if err != nil {
		return errors.ScyllaDBSaveError.DevMessage(err.Error())
	}

	return nil
}

// Get post by id from ScyllaDB
func (r *postCommandRepo) GetByID(id string) (*domain.Post, error) {

	var postData string
	var post domain.Post

	err := r.dbSession.Query("SELECT payload FROM posts WHERE id = ?", id).Scan(&postData)
	if err != nil {
		return nil, errors.ScyllaDBReadError.DevMessage(err.Error())
	}

	err = json.Unmarshal([]byte(postData), &post)
	if err != nil {
		return nil, errors.DeserializeBug.DevMessage(err.Error())
	}

	return &post, nil
}

// Delete post by id from ScyllaDB
func (r *postCommandRepo) DeleteById(id string) error {

	err := r.dbSession.Query("DELETE FROM posts WHERE id = ?", id).Exec()
	if err != nil {
		return errors.ScyllaDBSaveError.DevMessage(err.Error())
	}

	return nil
}






